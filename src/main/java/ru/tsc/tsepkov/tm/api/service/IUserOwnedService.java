package ru.tsc.tsepkov.tm.api.service;

import ru.tsc.tsepkov.tm.api.repository.IUserOwnedRepository;
import ru.tsc.tsepkov.tm.model.AbstractUserOwnedModel;

public interface IUserOwnedService<M extends AbstractUserOwnedModel> extends IUserOwnedRepository<M>, IService<M> {

}
