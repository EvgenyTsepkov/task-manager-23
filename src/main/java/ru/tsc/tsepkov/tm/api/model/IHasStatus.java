package ru.tsc.tsepkov.tm.api.model;

import org.jetbrains.annotations.Nullable;
import ru.tsc.tsepkov.tm.enumerated.Status;

public interface IHasStatus {

    @Nullable
    Status getStatus();

    void setStatus(@Nullable Status status);

}
