package ru.tsc.tsepkov.tm.enumerated;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.tsepkov.tm.comparator.CreatedComparator;
import ru.tsc.tsepkov.tm.comparator.NameComparator;
import ru.tsc.tsepkov.tm.comparator.StatusComparator;
import ru.tsc.tsepkov.tm.model.Task;

import java.util.Comparator;

public enum TaskSort {

    BY_NAME("Sort by name", NameComparator.INSTANCE::compare),
    BY_STATUS("Sort by status", StatusComparator.INSTANCE::compare),
    BY_CREATED("Sort by created", CreatedComparator.INSTANCE::compare),
    BY_DEFAULT("Sort by default", null);

    @Getter
    @NotNull
    private final String name;

    @Getter
    @Nullable
    private final Comparator<Task> comparator;

    @NotNull
    public static TaskSort toSort(@Nullable final String value) {
        if (value == null || value.isEmpty()) return BY_DEFAULT;
        for (@NotNull final TaskSort sort : values()) {
            if (sort.name().equals(value)) return sort;
        }
        return BY_DEFAULT;
    }

    TaskSort(@NotNull final String name, @Nullable Comparator<Task> comparator) {
        this.name = name;
        this.comparator = comparator;
    }

}
