package ru.tsc.tsepkov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.tsepkov.tm.enumerated.ProjectSort;
import ru.tsc.tsepkov.tm.model.Project;
import ru.tsc.tsepkov.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

public final class ProjectListCommand extends AbstractProjectCommand {

    @NotNull
    public static final String NAME = "project-list";

    @NotNull
    public static final String DESCRIPTION = "Show list projects.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[PROJECT LIST]");
        System.out.println("ENTER SORT:");
        System.out.println(Arrays.toString(ProjectSort.values()));
        @NotNull final String sortType = TerminalUtil.nextLine();
        @NotNull final ProjectSort sort = ProjectSort.toSort(sortType);
        @NotNull final String userId = getUserId();
        @NotNull final List<Project> projects = getProjectService().findAll(userId, sort.getComparator());
        int index = 1;
        for (@Nullable final Project project: projects) {
            if (project == null) continue;
            System.out.println(index + ". " + project.getName() + " : " + project.getDescription());
            index++;
        }
    }

}

