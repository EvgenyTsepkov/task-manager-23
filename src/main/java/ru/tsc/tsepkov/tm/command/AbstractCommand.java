package ru.tsc.tsepkov.tm.command;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.tsepkov.tm.api.model.ICommand;
import ru.tsc.tsepkov.tm.api.service.IServiceLocator;
import ru.tsc.tsepkov.tm.enumerated.Role;

public abstract class AbstractCommand implements ICommand {

    @Getter
    @Setter
    @NotNull
    protected IServiceLocator serviceLocator;

    @Nullable
    public abstract String getArgument();

    @Nullable
    public abstract String getName();

    @Nullable
    public abstract String getDescription();

    public abstract void execute();

    @NotNull
    public String getUserId() {
        return serviceLocator.getAuthService().getUserId();
    }

    public String toString() {
        @Nullable final String name = getName();
        @Nullable final String argument = getArgument();
        @Nullable final String description = getDescription();
        String displayName = "";
        if (name != null && !name.isEmpty()) displayName += name;
        if (argument != null && !argument.isEmpty()) displayName += ", " + argument;
        if (description != null && !description.isEmpty()) displayName += ": " + description;
        return displayName;
    }

    @Nullable
    public abstract Role[] getRoles();

}
