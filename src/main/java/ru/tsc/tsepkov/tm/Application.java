package ru.tsc.tsepkov.tm;

import org.jetbrains.annotations.NotNull;
import ru.tsc.tsepkov.tm.component.Bootstrap;

public final class Application {

    public static void main(final String[] args) {
        @NotNull final Bootstrap bootstrap = new Bootstrap();
        bootstrap.start(args);
    }

}
