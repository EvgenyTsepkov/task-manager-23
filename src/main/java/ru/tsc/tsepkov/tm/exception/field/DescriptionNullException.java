package ru.tsc.tsepkov.tm.exception.field;

public final class DescriptionNullException extends AbstractFieldException {

    public DescriptionNullException() {
        super("Error! Description is null...");
    }

}
